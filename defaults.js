function defaults(obj, defaultProps)
{
    const objClone= { ...obj}
    for(key in defaultProps){
        objClone[key]=defaultProps[key]
    }
    return objClone
}

module.exports=defaults